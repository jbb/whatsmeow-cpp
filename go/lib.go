package main

import (
    "fmt"
    "unsafe"
    "context"
    
    _ "github.com/mattn/go-sqlite3"
    "go.mau.fi/whatsmeow"
    "go.mau.fi/whatsmeow/store"
    "go.mau.fi/whatsmeow/types"
    "go.mau.fi/whatsmeow/store/sqlstore"
    "go.mau.fi/whatsmeow/types/events"
    "go.mau.fi/whatsmeow/binary/proto"
    "github.com/mattn/go-pointer"
)

//#include "types.h"
import "C"

//export new_container
func new_container(filename string) C.Container {
    store, err := sqlstore.New("sqlite3", fmt.Sprintf("file:%s?_foreign_keys=on", filename), nil)
    if err != nil {
        fmt.Printf("%s", err)
    }

    return C.Container {
        d: pointer.Save(store),
    }
}

//export container_get_first_device
func container_get_first_device(container C.Container) C.Device {
    deviceStore, err := pointer.Restore(container.d).(*sqlstore.Container).GetFirstDevice()

    if err != nil {
        fmt.Printf("%s", err)
    }
    return C.Device {
        d: pointer.Save(deviceStore),
    }
}

//export new_client
func new_client(deviceStore C.Device) C.Client {
    client := whatsmeow.NewClient(pointer.Restore(deviceStore.d).(*store.Device), nil)
    return C.Client {
        d: pointer.Save(client),
    }
}

func handleEvent(eventType uint32, event unsafe.Pointer, handler C.EventHandler) {
    cEvent := C.Event {
        eventType: eventType,
        event: *((*[8]byte)(event)),
    }
    C.callEventHandler(handler, cEvent)
}

//export client_add_event_handler
func client_add_event_handler(client C.Client, eventHandler C.EventHandler) uint32 {
    return pointer.Restore(client.d).(*whatsmeow.Client).AddEventHandler(func(evt interface{}) {
        switch v := evt.(type) {
            case *events.Message: {
                handleEvent(C.EventTypeMessage, (unsafe.Pointer)(&C.MessageEvent {
                    d: pointer.Save(v),
                }), eventHandler)
            }
            case *events.Connected: {
                handleEvent(C.EventTypeConnected, (unsafe.Pointer)(&C.ConnectedEvent {
                    d: pointer.Save(v),
                }), eventHandler)
            }
            case *events.HistorySync: {
                handleEvent(C.EventTypeHistorySync, (unsafe.Pointer)(&C.HistorySyncEvent {
                    d: pointer.Save(v),
                }), eventHandler)
            }
            case *events.LoggedOut: {
                handleEvent(C.EventTypeLoggedOut, (unsafe.Pointer)(&C.LoggedOutEvent {
                    d: pointer.Save(v),
                }), eventHandler)
            }
            case *events.OfflineSyncCompleted: {
                handleEvent(C.EventTypeOfflineSyncCompleted, (unsafe.Pointer)(&C.OfflineSyncCompletedEvent {
                    d: pointer.Save(v),
                }), eventHandler)
            }
        }
    })
}

//export client_remove_event_handler
func client_remove_event_handler(client C.Client, id uint32) bool {
    return pointer.Restore(client.d).(*whatsmeow.Client).RemoveEventHandler(id)
}

//export message_event_get_message
func message_event_get_message(event C.MessageEvent) C.Message {
    return C.Message {
        d: pointer.Save(pointer.Restore(event.d).(*events.Message).Message),
    }
}

//export message_get_conversation
func message_get_conversation(message C.Message) *C.char {
    return C.CString(pointer.Restore(message.d).(*proto.Message).GetConversation())
}

//export message_event_get_chat
func message_event_get_chat(message C.MessageEvent) C.Chat {
    msg := pointer.Restore(message.d).(*events.Message);
    chat := msg.Message.Chat;

    if chat == nil {
        fmt.Println("message_event_get_chat: Chat is null")
    }

    return C.Chat {
        d: pointer.Save(chat),
    }
}

//export chat_is_null
func chat_is_null(chat C.Chat) bool {
    return pointer.Restore(chat.d).(*proto.Chat) == nil
}

//export chat_get_display_name
func chat_get_display_name(chat C.Chat) *C.char {
    c := pointer.Restore(chat.d).(*proto.Chat)
    name := c.DisplayName;
    if name == nil {
        return C.CString("")
    }
    return C.CString(*name)
}

//export client_get_qr_channel
func client_get_qr_channel(client C.Client) C.QrChannel {
    c := pointer.Restore(client.d).(*whatsmeow.Client)
    qrChan, _ := c.GetQRChannel(context.Background())
    
    return C.QrChannel {
        d: pointer.Save(&qrChan),
    }
}

//export client_connect
func client_connect(client C.Client) {
    pointer.Restore(client.d).(*whatsmeow.Client).Connect()
}

//export qr_channel_poll
func qr_channel_poll(channel C.QrChannel) C.QrChannelResult {
    c := *(pointer.Restore(channel.d).(*<-chan whatsmeow.QRChannelItem))
    select {
        case msg := <- c:
            return C.QrChannelResult {
                item: (*C.QrChannelItem)(pointer.Save(&C.QrChannelItem {
                    d: pointer.Save(&msg),
                })),
            }
        default:
            return C.QrChannelResult {
                item: (*C.QrChannelItem)(unsafe.Pointer(nil)),
            }
    }
}

//export qr_channel_item_get_code
func qr_channel_item_get_code(item *C.QrChannelItem) *C.char {
    i := pointer.Restore((unsafe.Pointer)(item)).(*C.QrChannelItem)
    return C.CString(pointer.Restore(i.d).(*whatsmeow.QRChannelItem).Code)
}

//export qr_channel_item_get_event
func qr_channel_item_get_event(item *C.QrChannelItem) *C.char {
    i := pointer.Restore((unsafe.Pointer)(item)).(*C.QrChannelItem)
    return C.CString(pointer.Restore(i.d).(*whatsmeow.QRChannelItem).Event)
}

//export client_get_device
func client_get_device(client C.Client) C.Device {
    store := pointer.Restore(client.d).(*whatsmeow.Client).Store
    return C.Device { d: pointer.Save(store), }
}

//export device_get_id
func device_get_id(device C.Device) C.JID {
    id := pointer.Restore(device.d).(*store.Device).ID
    return C.JID { d: pointer.Save(id) }
}

//export jid_is_null
func jid_is_null(jid C.JID) bool {
    return pointer.Restore(jid.d).(*types.JID) == nil
}

//export client_is_connected
func client_is_connected(client C.Client) bool {
    return pointer.Restore(client.d).(*whatsmeow.Client).IsConnected()
}

//export client_disconnect
func client_disconnect(client C.Client) {
    pointer.Restore(client.d).(*whatsmeow.Client).Disconnect()
}

//export client_logout
func client_logout(client C.Client) {
    pointer.Restore(client.d).(*whatsmeow.Client).Logout()
}

//export client_is_logged_in
func client_is_logged_in(client C.Client) bool {
    return pointer.Restore(client.d).(*whatsmeow.Client).IsLoggedIn()
}

//export client_send_message
func client_send_message(client C.Client, to C.JID, message string) {
    jid := pointer.Restore(to.d).(*types.JID)
    pointer.Restore(client.d).(*whatsmeow.Client).SendMessage(*jid, "", &proto.Message {
        Conversation: &message,
    })
}

//export client_send_presence
func client_send_presence(client C.Client, presence C.Presence) {
    var presenceString types.Presence
    switch presence {
        case C.PresenceAvailable:
            presenceString = types.PresenceAvailable
        case C.PresenceUnavailable:
            presenceString = types.PresenceUnavailable
    }
    pointer.Restore(client.d).(*whatsmeow.Client).SendPresence(presenceString)
}

//export jid_to_string
func jid_to_string(jid C.JID) *C.char {
    return C.CString(pointer.Restore(jid.d).(*types.JID).String())
}

//export client_set_proxy_address
func client_set_proxy_address(client C.Client, address string) {
    pointer.Restore(client.d).(*whatsmeow.Client).SetProxyAddress(address)
}

//export client_free
func client_free(client C.Client) {
    pointer.Unref(client.d)
}

//export device_free
func device_free(device C.Device) {
    pointer.Unref(device.d)
}

//export container_free
func container_free(container C.Container) {
    pointer.Unref(container.d)
}

//export chat_free
func chat_free(chat C.Chat) {
    pointer.Unref(chat.d)
}

//export message_free
func message_free(message C.Message) {
    pointer.Unref(message.d)
}

//export private_d_free
func private_d_free(event unsafe.Pointer) {
    pointer.Unref(event)
}

//export jid_free
func jid_free(jid C.JID) {
    pointer.Unref(jid.d)
}

//export qr_channel_item_free
func qr_channel_item_free(item *C.QrChannelItem) {
    pointer.Unref(item.d)
}

//export qr_channel_free
func qr_channel_free(channel C.QrChannel) {
    pointer.Unref(channel.d)
}

//export qr_channel_item_is_success
func  qr_channel_item_is_success(item *C.QrChannelItem) bool {
    return *pointer.Restore(item.d).(*whatsmeow.QRChannelItem) == whatsmeow.QRChannelSuccess
}

//export qr_channel_item_is_timeout
func  qr_channel_item_is_timeout(item *C.QrChannelItem) bool {
    return *pointer.Restore(item.d).(*whatsmeow.QRChannelItem) == whatsmeow.QRChannelTimeout
}

//export qr_channel_item_is_err_unexpected_event
func qr_channel_item_is_err_unexpected_event(item *C.QrChannelItem) bool {
    return *pointer.Restore(item.d).(*whatsmeow.QRChannelItem) == whatsmeow.QRChannelErrUnexpectedEvent
}

//export qr_channel_item_is_client_outdated
func qr_channel_item_is_client_outdated(item *C.QrChannelItem) bool {
    return *pointer.Restore(item.d).(*whatsmeow.QRChannelItem) == whatsmeow.QRChannelClientOutdated
}

//export qr_channel_item_is_scanned_without_multi_device
func qr_channel_item_is_scanned_without_multi_device(item *C.QrChannelItem) bool {
    return *pointer.Restore(item.d).(*whatsmeow.QRChannelItem) == whatsmeow.QRChannelScannedWithoutMultidevice
}

//export qr_channel_item_is_code
func qr_channel_item_is_code(item *C.QrChannelItem) bool {
    return pointer.Restore(item.d).(*whatsmeow.QRChannelItem).Event == "code"
}

func main() {}

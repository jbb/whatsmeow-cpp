extern crate libc;

#[macro_use]
extern crate lazy_static;

use std::borrow::Cow;
use std::cell::RefCell;
use std::sync::Mutex;

lazy_static! {
    static ref CALLBACK: Mutex<RefCell<Option<(Box<dyn FnMut(Event) + Send>, u32)>>> =
        Mutex::from(RefCell::new(None));
}

use std::ffi::{CStr, CString};

mod ffi {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(dead_code)]

    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

struct GString {
    cstring: CString,
    len: usize,
}

impl GString {
    fn new(text: &str) -> GString {
        GString {
            cstring: CString::new(text).unwrap(),
            len: text.len(),
        }
    }

    fn as_gstr(&self) -> ffi::GoString {
        ffi::GoString {
            p: self.cstring.as_ptr(),
            n: self.len as isize,
        }
    }
}

/// A database of whatsapp data
pub struct Container {
    inner: ffi::Container,
}

impl Container {
    /// Open / Create a database at the given path
    pub fn new(location: &str) -> Container {
        let gstring = GString::new(location);
        Container {
            inner: unsafe { ffi::new_container(gstring.as_gstr()) },
        }
    }

    pub fn get_first_device(&self) -> Device {
        return Device {
            inner: unsafe { ffi::container_get_first_device(self.inner) },
        };
    }
}

impl Drop for Container {
    fn drop(&mut self) {
        unsafe { ffi::container_free(self.inner) }
    }
}

/// The jabber id of a device / user
pub struct Jid {
    inner: ffi::JID,
}

impl Jid {
    fn is_null(&self) -> bool {
        unsafe { ffi::jid_is_null(self.inner) != 0 }
    }
}

impl ToString for Jid {
    /// Convert the jid to a string representation
    fn to_string(&self) -> String {
        let cstr = unsafe { ffi::jid_to_string(self.inner) };
        let cstr = unsafe { CStr::from_ptr(cstr) };
        cstr.to_string_lossy().to_string()
    }
}

impl Drop for Jid {
    fn drop(&mut self) {
        unsafe { ffi::jid_free(self.inner) }
    }
}

/// A registered WhatsApp device
pub struct Device {
    inner: ffi::Device,
}

impl Device {
    /// Get the full jid associated with the device
    pub fn get_id(&self) -> Option<Jid> {
        let jid = Jid {
            inner: unsafe { ffi::device_get_id(self.inner) },
        };

        if jid.is_null() {
            None
        } else {
            Some(jid)
        }
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe { ffi::device_free(self.inner) }
    }
}

/// A single item that can be retrieved from a QR-Channel
pub struct QrChannelItem {
    inner: *mut ffi::QrChannelItem,
}

impl<'a> QrChannelItem {
    /// Get the type of event as string
    pub fn get_event(&self) -> Cow<'a, str> {
        let cstr = unsafe { ffi::qr_channel_item_get_event(self.inner) };
        let cstr = unsafe { CStr::from_ptr(cstr) };
        cstr.to_string_lossy()
    }

    /// Get the text that should be encoded as a QR Code
    pub fn get_code(&self) -> Cow<'a, str> {
        let cstr = unsafe { ffi::qr_channel_item_get_code(self.inner) };
        unsafe { CStr::from_ptr(cstr) }.to_string_lossy()
    }

    pub fn is_success(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_success(self.inner) != 0 }
    }

    pub fn is_timeout(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_timeout(self.inner) != 0 }
    }

    pub fn is_err_unexpected_event(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_err_unexpected_event(self.inner) != 0 }
    }

    pub fn is_client_outdated(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_client_outdated(self.inner) != 0 }
    }

    pub fn is_scanned_without_multi_device(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_scanned_without_multi_device(self.inner) != 0 }
    }

    pub fn is_code(&self) -> bool {
        unsafe { ffi::qr_channel_item_is_code(self.inner) != 0 }
    }
}

impl Drop for QrChannelItem {
    fn drop(&mut self) {
        unsafe { ffi::qr_channel_item_free(self.inner) }
    }
}

/// Channel to retrieve a QR-Code from
pub struct QrChannel {
    inner: ffi::QrChannel,
}

impl QrChannel {
    // Check whether a QR code is available
    pub fn poll(&self) -> Option<QrChannelItem> {
        let result = unsafe { ffi::qr_channel_poll(self.inner) };
        if result.item.is_null() {
            None
        } else {
            Some(QrChannelItem { inner: result.item })
        }
    }
}

impl Drop for QrChannel {
    fn drop(&mut self) {
        unsafe { ffi::qr_channel_free(self.inner) }
    }
}

/// The WhatsApp Client
pub struct Client {
    inner: ffi::Client,
}

/// A single WhatsApp message
pub struct Message {
    inner: ffi::Message,
}

impl<'a> Message {
    // Get the text of the message
    pub fn get_conversation(&self) -> Cow<'a, str> {
        unsafe { CStr::from_ptr(ffi::message_get_conversation(self.inner)) }.to_string_lossy()
    }
}

impl Drop for Message {
    fn drop(&mut self) {
        unsafe { ffi::message_free(self.inner) };
    }
}

/// Event that is emitted when a Message arrives
pub struct MessageEvent {
    inner: ffi::MessageEvent,
}

impl MessageEvent {
    /// Retrieve the message that is contained in the MessageEvent
    pub fn get_message(&self) -> Message {
        Message {
            inner: unsafe { ffi::message_event_get_message(self.inner) },
        }
    }
}

impl Drop for MessageEvent {
    fn drop(&mut self) {
        unsafe { ffi::private_d_free(self.inner.d) }
    }
}

/// Event that is emitted when the client has successfully connected
pub struct ConnectedEvent {
    inner: ffi::ConnectedEvent,
}

impl Drop for ConnectedEvent {
    fn drop(&mut self) {
        unsafe { ffi::private_d_free(self.inner.d) }
    }
}

/// Event that is emitted when a new history sync package arrived
pub struct HistorySyncEvent {
    inner: ffi::HistorySyncEvent,
}

impl Drop for HistorySyncEvent {
    fn drop(&mut self) {
        unsafe { ffi::private_d_free(self.inner.d) }
    }
}

/// Event that is emitted when the client has logged out
pub struct LoggedOutEvent {
    inner: ffi::LoggedOutEvent,
}

impl Drop for LoggedOutEvent {
    fn drop(&mut self) {
        unsafe { ffi::private_d_free(self.inner.d) }
    }
}

/// Event that is emitted when a new history sync is finished
pub struct OfflineSyncCompletedEvent {
    inner: ffi::OfflineSyncCompletedEvent,
}

impl Drop for OfflineSyncCompletedEvent {
    fn drop(&mut self) {
        unsafe { ffi::private_d_free(self.inner.d) }
    }
}

/// Events that
pub enum Event {
    Message(MessageEvent),
    Connected(ConnectedEvent),
    HistorySync(HistorySyncEvent),
    LoggedOut(LoggedOutEvent),
    OfflineSyncCompleted(OfflineSyncCompletedEvent),
}

/// Presence of the user
pub enum Presence {
    Available,
    Unavailable,
}

unsafe extern "C" fn call_handler(event: ffi::Event) {
    let cb = CALLBACK.lock().unwrap();
    let mut cb = (*cb).borrow_mut();
    if let Some((ref mut cb, _)) = *cb {
        let event = match event.eventType {
            ffi::Event_EventTypeMessage => Event::Message(MessageEvent {
                inner: event.event.message,
            }),
            ffi::Event_EventTypeConnected => Event::Connected(ConnectedEvent {
                inner: event.event.connected,
            }),
            ffi::Event_EventTypeHistorySync => Event::HistorySync(HistorySyncEvent {
                inner: event.event.history_sync,
            }),
            ffi::Event_EventTypeLoggedOut => Event::LoggedOut(LoggedOutEvent {
                inner: event.event.logged_out,
            }),
            ffi::Event_EventTypeOfflineSyncCompleted => {
                Event::OfflineSyncCompleted(OfflineSyncCompletedEvent {
                    inner: event.event.offline_sync_completed,
                })
            }
            _ => unreachable!("Unhandled event type"),
        };

        cb(event);
    }
}

impl Client {
    /// Initializes a new WhatsApp Client
    pub fn new(device: &Device) -> Client {
        Client {
            inner: unsafe { ffi::new_client(device.inner) },
        }
    }

    /// Set the function that should be called when an event arrives
    pub fn set_event_handler<HandlerFn>(&self, handler: HandlerFn) -> u32
    where
        HandlerFn: FnMut(Event) + 'static + Send + Sync,
    {
        let id = unsafe { ffi::client_add_event_handler(self.inner, Some(call_handler)) };

        let c = Box::from(handler);
        CALLBACK.lock().unwrap().replace(Some((c, id)));

        return id;
    }

    // Remove the event handler
    pub fn remove_event_handler(&self) {
        if let Some((_, id)) = *CALLBACK.lock().unwrap().borrow() {
            unsafe {
                ffi::client_remove_event_handler(self.inner, id);
            }
        }
    }

    /// Get the device object associated with the client
    pub fn get_device(&self) -> Device {
        Device {
            inner: unsafe { ffi::client_get_device(self.inner) },
        }
    }

    /// Request a new login Qr-Code and returns a chanel from which it can be retrieved when ready
    pub fn get_qr_channel(&self) -> QrChannel {
        QrChannel {
            inner: unsafe { ffi::client_get_qr_channel(self.inner) },
        }
    }

    /// Connect to the WhatsApp server
    pub fn connect(&self) {
        unsafe {
            ffi::client_connect(self.inner);
        }
    }

    /// Disconnect to the WhatsApp server
    pub fn disconnect(&self) {
        unsafe {
            ffi::client_disconnect(self.inner);
        }
    }

    /// Set whether the client is actively online
    pub fn send_presence(&self, presence: Presence) {
        unsafe {
            ffi::client_send_presence(self.inner, presence as u32);
        }
    }

    /// Check whether the client is connected
    pub fn is_connected(&self) -> bool {
        unsafe { ffi::client_is_connected(self.inner) != 0 }
    }

    /// Check whether the client is logged in
    pub fn is_logged_in(&self) -> bool {
        unsafe { ffi::client_is_logged_in(self.inner) != 0 }
    }

    /// Set a proxy address that is to be used when connecting to the WhatsApp server
    pub fn set_proxy_address(&mut self, address: &str) {
        let gstring = GString::new(address);
        unsafe {
            ffi::client_set_proxy_address(self.inner, gstring.as_gstr());
        }
    }

    /// Send a message to the given jid
    pub fn send_message(&self, jid: Jid, message: &str) {
        let gstring = GString::new(message);
        unsafe { ffi::client_send_message(self.inner, jid.inner, gstring.as_gstr()) };
    }

    /// Log out
    pub fn logout(&self) {
        unsafe { ffi::client_logout(self.inner) }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        unsafe { ffi::client_free(self.inner) }
    }
}

#include "types.h"

void callEventHandler(EventHandler fn, Event event) {
   fn(event);
}

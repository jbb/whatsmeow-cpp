use std::time::Duration;
use whatsmeow::*;

fn main() {
    let store = Container::new("./db.sqlite");
    let device = store.get_first_device();
    let client = Client::new(&device);
    client.set_event_handler(|event: Event| match event {
        Event::Message(m) => {
            println!("Received Message: {}", m.get_message().get_conversation());
        }
        Event::Connected(_c) => {
            println!("ConnectedEvent");
        }
        _ => {}
    });

    if device.get_id().is_none() {
        let channel = client.get_qr_channel();
        client.connect();

        loop {
            if let Some(item) = channel.poll() {
                if item.is_code() {
                    println!("{}", item.get_code());
                    break;
                } else {
                    println!("Login Event");
                    break;
                }
            }
        }
    } else {
        println!("Connecting");
        client.connect();

        println!("Connected: {}.", client.is_connected());
    }

    println!("Sending presence");
    client.send_presence(Presence::Available);

    if let Some(jid) = device.get_id() {
        println!("My jid is {}", jid.to_string());
    }

    println!("Waiting for events.");

    loop {
        std::thread::sleep(Duration::from_millis(400));
    }


    client.disconnect();
}

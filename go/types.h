typedef struct {
    void* d;
} Container;

typedef struct {
    void *d;
} Device;

typedef struct {
    void *d;
} Client;

typedef struct {
    void *d;
} JID;

typedef struct {
    void *d;
} QrChannel;

typedef struct {
    void *d;
} QrChannelItem;

typedef struct {
    QrChannelItem *item;
} QrChannelResult;

typedef enum {
    PresenceAvailable,
    PresenceUnavailable
} Presence;

typedef struct {
    void *d;
} Chat;

typedef struct {
    void *d;
} Message;

// Events
typedef struct {
    void *d;
} MessageEvent;

typedef struct {
    void *d;
} ConnectedEvent;

typedef struct {
    void *d;
} HistorySyncEvent;

typedef struct {
    void *d;
} LoggedOutEvent;

typedef struct {
    void *d;
} OfflineSyncCompletedEvent;

typedef struct {
    enum {
        EventTypeMessage,
        EventTypeConnected,
        EventTypeHistorySync,
        EventTypeLoggedOut,
        EventTypeOfflineSyncCompleted
    } eventType;
    union {
        MessageEvent message;
        ConnectedEvent connected;
	HistorySyncEvent history_sync;
	LoggedOutEvent logged_out;
	OfflineSyncCompletedEvent offline_sync_completed;
    } event;
} Event;

typedef void (*EventHandler)(Event);

void callEventHandler(EventHandler fn, Event event);

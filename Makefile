WHATSMEOW_AR = go/libwhatsmeow.a
CPP_CLIENT   = cpp-client
RUST_CLIENT  = whatsmeow-rs/target/debug/whatsmeow-rust-client

all: $(WHATSMEOW_AR) $(CPP_CLIENT) $(RUST_CLIENT)

$(CPP_CLIENT): $(WHATSMEOW_AR) main.cpp
	g++ main.cpp -Igo -Lgo -lwhatsmeow -lpthread -ldl -o $(CPP_CLIENT)

$(RUST_CLIENT): whatsmeow-rs/src/*.rs whatsmeow-rs/*.toml $(WHATSMEOW_AR)
	$(MAKE) -C whatsmeow-rs

$(WHATSMEOW_AR): go/*.go go/*.h go/*.c go/*.mod go/*.sum
	$(MAKE) -C go

run-cpp: $(CPP_CLIENT)
	./cpp-client

run-rs: $(RUST_CLIENT)
	./$(RUST_CLIENT)

clean:
	rm -rf $(CPP_CLIENT)
	$(MAKE) -C go clean
	$(MAKE) -C whatsmeow-rs clean

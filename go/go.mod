module whatsmeow-ffi

go 1.17

require (
	github.com/mattn/go-pointer v0.0.1
	github.com/mattn/go-sqlite3 v1.14.12
	go.mau.fi/whatsmeow v0.0.0-20220309174443-4ea4925be30c
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	go.mau.fi/libsignal v0.0.0-20220308120827-0d87a03fd7c7 // indirect
	golang.org/x/crypto v0.0.0-20220307211146-efcb8507fb70 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

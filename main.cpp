#include "libwhatsmeow.h"

#include <string_view>
#include <iostream>
#include <cassert>
#include <unistd.h>

class GoStringView : public GoString {
public:
    constexpr GoStringView(GoString string) {
        p = string.p;
        n = string.n;
    }

    constexpr GoStringView(std::string_view view) {
        p = view.data();
        n = view.size();
    }

    constexpr GoStringView(const char *text)
        : GoStringView(std::string_view(text))
    {
    }

    constexpr operator std::string_view() {
        return std::string_view(p, n);
    }
};

inline GoStringView operator"" _gs(const char *str, size_t size) noexcept {
    return GoStringView(std::string_view(str, size));
}

int main() {
    auto store = new_container("./db.sqlite"_gs);
    auto device = container_get_first_device(store);
    auto client = new_client(device);
    client_add_event_handler(client, [](Event event) {
        switch (event.eventType) {
        case Event::EventTypeMessage: {
            auto message_event = event.event.message;
            auto message = message_event_get_message(message_event);
            auto chat = message_event_get_chat(message_event);
            if (!chat_is_null(chat)) {
                std::cout << "From: " << chat_get_display_name(chat) << std::endl;
            }
            std::cout << "Text: " << message_get_conversation(message) << std::endl;
            std::flush(std::cout);
            break;
        }
        case Event::EventTypeConnected:
            std::cout << "ConnectedEvent" << std::endl;
            std::flush(std::cout);
            break;
        }
    });

    if (jid_is_null(device_get_id(client_get_device(client)))) {
        auto channel = client_get_qr_channel(client);
        client_connect(client);

        while (true) {
            auto result = qr_channel_poll(channel);
            if (result.item) {
                std::string_view event = qr_channel_item_get_event(result.item);
                if (event == "code") {
                    std::cout << qr_channel_item_get_code(result.item);
                    std::flush(std::cout);
                } else {
                    std::cout << "Login event" << event;
                    std::flush(std::cout);
                    break;
                }
            }
        }
    } else {
        std::cout << "Connecting" << std::endl;
        std::flush(std::cout);
        client_connect(client);
        std::cout << "Connected: " << bool(client_is_connected(client)) << "." << std::endl;
        std::flush(std::cout);
    }
    
    std::cout << "Sending presence" << std::endl;
    client_send_presence(client, Presence::PresenceAvailable);

    std::cout << "My jid is " << jid_to_string(device_get_id(device)) << std::endl;

    std::cout << "Waiting for events" << "." << std::endl;
    std::flush(std::cout);

    // We can block herer or do whatever, since goroutines run on other threads
    while (true) {
        sleep(400);
    }

    client_disconnect(client);

    std::cout << "Bye";
}
